(function ($) {
  'use strict';
  Drupal.behaviors.iconPicker = {
    attach: function (context) {
      // Loop through each icon field.
      $('.icon-picker-icons-list', context).each(function () {

        // See if we can replace the select with the chosen icon.
        var $form_item = $(this).closest('.field-icon-selector');

        // Hide the original select.
        var $select = $form_item.find('select.icon-picker');
        $select.hide();

        // Add a selection preview area.
        if (!$(this).attr('initialized')) {
          $select.before("<a class='selection-preview' style='display: inline-block; zoom: 2; vertical-align: middle; cursor: pointer; padding: 2px; border: 0.5px dotted grey; margin-right: 3px;'></a>");
          // Add a button to remove the selection.
          $(" <span class='remove-icon' style='margin-right: 1em; cursor: pointer;'><a> " + Drupal.t('Remove selection') + ' </a></span>').insertAfter($select);
        }
        var selectionPreview = $select.prev('.selection-preview');

        // Find the items we need.
        var $iconList = $form_item.find('.icon-picker-icons-list');

        // Add close button.
        var $close_button = "<span class='close' style='display: block; float: right; border: 1px solid; padding: .3em; cursor: pointer;'>" + Drupal.t('Close') + '</span>';

        if (!$(this).attr('initialized')) {
          // Add search function
          $iconList.prepend($close_button + '<form><label>' + Drupal.t('Search:') + '</label> <input class="form-text icon-search" type="text" /></form>');
        }
        // Handle close button.
        $form_item.find('.close').click(function () {
          $(this).closest('.icon-picker-icons-list').hide();
        });

        $iconList.find('.icon-search').keyup(search);

        function search() {
          var $search = $(this).val();
          var $items = $(this).closest('.icon-picker-icons-list');
          if ($search) {
            // Show the labels when searching.
            $('.label', $items).show();

            // loop through the bundles.
            $('.bundle', $items).each(function () {
              var $count = 0;
              $('.icon-wrapper', $(this)).each(function () {
                var $item = $(this);
                var $name = $item.find('[data-title]').attr('data-title');
                if ($name) {
                  if ($name.toLowerCase().indexOf($search.toLowerCase()) >= 0) {
                    $count++;
                  }
                  else {
                    $item.fadeOut();
                  }
                }
              });
              // Check if we don't have any bundles without result. If so hide the whole bundle.
              if ($count > 0) {
                $(this).show();
              }
              else {
                $(this).hide();
              }
            });
          }
          else {
            // nothing to search for, make sure everything is hidden / visible (again).
            $('.icon-wrapper', $items).show();
            $('bundle', $items).show();
            $('.label', $items).hide();
          }
        }

        // Make the selected icon visible
        function showSelection(item) {
          var $icon_value = $select.val();

          // Reset the preview
          selectionPreview.empty();

          // If and item was selected show it in the preview.
          if ($icon_value) {
            var icon = $("[data-select-name='" + $icon_value + "']", $iconList).clone();
            icon.appendTo(selectionPreview);
            $('.remove-icon', item).show();
            $iconList.fadeOut();

            // If we are in a menu settings, restore the hidden dependent fields:
            $('.form-item-options-icon-position, ' +
            '.form-item-options-icon-breadcrumb, ' +
            '.form-item-options-icon-title-wrapper, ' +
            '.form-item-options-icon-title-wrapper-element,' +
            '.form-item-options-icon-title-wrapper-class', item).addClass('menu-icon-options').show();
          }
          // No selection, hide the 'hide' button.
          else {
            $('.remove-icon', item).hide();
            selectionPreview.html('<span style="zoom:0.5; padding: 0.2em 1em; color: grey;">' + Drupal.t('no icon selected') + '</span>');
            // Hide the dependent fields again:
            $('.menu-icon-options', item).hide();
            $iconList.hide();
          }
        }

        // If an item is already selected, make it visible.
        showSelection($form_item);

        // Show the available icons, when clicking on the preview area
        $('.selection-preview', $form_item).click(function () {
          $('.icon-picker-icons-list', $form_item).fadeIn();
        });

        // Remove the selected icon
        $('.remove-icon', $form_item).click(function () {
          $select.val('');
          $('.icon-picker-icons-list', $form_item).fadeIn();
          $('.selected', $form_item).removeClass('selected');
          showSelection($form_item);
        });

        // Handle licking on an icon.
        $('.icon-wrapper', this).click(function () {
          // Find the matching form item and select.
          var $form_item = $(this).closest('.field-icon-selector');
          var $select = $form_item.find('select');

          // Update the select field
          // var selectedIcon = $(this).find(".icon").attr("data-select-name");
          var selectedIcon = $(this).find('[data-select-name]').attr('data-select-name');

          $select.val(selectedIcon);

          // Update the preview
          showSelection($form_item);
        });

        $(this).attr('initialized', 1);
      });
    }
  };
})(jQuery);
